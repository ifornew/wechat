<?php

declare(strict_types=1);

namespace Ifornew\WeChat\MiniApp\Contracts;

interface Account
{
    public function getAppId(): string;

    public function getSecret(): string;

    public function getToken(): ?string;

    public function getAesKey(): ?string;
}
