<?php

declare(strict_types=1);

namespace Ifornew\WeChat\MiniApp;

class AccessToken extends \Ifornew\WeChat\OfficialAccount\AccessToken
{
    const CACHE_KEY_PREFIX = 'mini_app';
}
