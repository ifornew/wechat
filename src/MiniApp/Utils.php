<?php

namespace Ifornew\WeChat\MiniApp;

use Ifornew\WeChat\Kernel\Exceptions\DecryptException;
use Ifornew\WeChat\Kernel\Exceptions\HttpException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

class Utils
{
    protected Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param string $code
     * @return array
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws HttpException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function codeToSession(string $code): array
    {
        $response = $this->app->getHttpClient()->request('GET', '/sns/jscode2session', [
            'query' => [
                'appid'      => $this->app->getAccount()->getAppId(),
                'secret'     => $this->app->getAccount()->getSecret(),
                'js_code'    => $code,
                'grant_type' => 'authorization_code',
            ],
        ])->toArray(false);

        if (empty($response['openid'])) {
            throw new HttpException('code2Session error: ' . json_encode($response, JSON_UNESCAPED_UNICODE));
        }

        return $response;
    }

    /**
     * @param string $sessionKey
     * @param string $iv
     * @param string $cipherText
     * @return array
     * @throws DecryptException
     */
    public function decryptSession(string $sessionKey, string $iv, string $cipherText): array
    {
        return Decryptor::decrypt($sessionKey, $iv, $cipherText);
    }
}
