<?php

declare(strict_types=1);

namespace Ifornew\WeChat\MiniApp;

use Ifornew\WeChat\MiniApp\Contracts\Account as AccountInterface;

class Account extends \Ifornew\WeChat\OfficialAccount\Account implements AccountInterface
{
    //
}
