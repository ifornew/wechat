<?php

declare(strict_types=1);

namespace Ifornew\WeChat\MiniApp;

use Ifornew\WeChat\Kernel\Exceptions\DecryptException;
use Ifornew\WeChat\Kernel\Support\AesCbc;
use Throwable;

use function base64_decode;
use function is_array;
use function json_decode;
use function sprintf;

class Decryptor
{
    /**
     * @param string $sessionKey
     * @param string $iv
     * @param string $cipherText
     * @return array<string, mixed>
     *
     * @throws DecryptException
     */
    public static function decrypt(string $sessionKey, string $iv, string $cipherText): array
    {
        try {
            $decrypted = AesCbc::decrypt(
                $cipherText,
                base64_decode($sessionKey, false),
                base64_decode($iv, false)
            );

            $decrypted = json_decode($decrypted, true);

            if (! $decrypted || ! is_array($decrypted)) {
                throw new DecryptException('The given payload is invalid.');
            }
        } catch (Throwable $e) {
            throw new DecryptException(sprintf('The given payload is invalid: %s', $e->getMessage()));
        }

        return $decrypted;
    }
}
