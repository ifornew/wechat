<?php

namespace Ifornew\WeChat\Pay;

use Closure;
use Ifornew\WeChat\Kernel\Contracts\Server as ServerInterface;
use Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException;
use Ifornew\WeChat\Kernel\Exceptions\RuntimeException;
use Ifornew\WeChat\Kernel\HttpClient\RequestUtil;
use Ifornew\WeChat\Kernel\ServerResponse;
use Ifornew\WeChat\Kernel\Support\AesEcb;
use Ifornew\WeChat\Kernel\Support\AesGcm;
use Ifornew\WeChat\Kernel\Support\Xml;
use Ifornew\WeChat\Kernel\Traits\InteractWithHandlers;
use Ifornew\WeChat\Pay\Contracts\Merchant as MerchantInterface;
use Exception;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use function is_array;
use function json_decode;
use function json_encode;
use function strval;

/**
 * @link https://pay.weixin.qq.com/wiki/doc/apiv3/wechatpay/wechatpay4_1.shtml
 * @link https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_5.shtml
 */
class Server implements ServerInterface
{
    use InteractWithHandlers;

    protected MerchantInterface $merchant;

    protected ServerRequestInterface $request;

    /**
     * @param MerchantInterface $merchant
     * @param ServerRequestInterface|null $request
     */
    public function __construct(MerchantInterface $merchant, ?ServerRequestInterface $request)
    {
        $this->merchant = $merchant;
        $this->request = $request ?? RequestUtil::createDefaultServerRequest();
    }

    /**
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function serve(): ResponseInterface
    {
        $message = $this->getRequestMessage();

        try {
            $defaultResponse = new Response(200, [], strval(json_encode(['code' => 'SUCCESS', 'message' => '成功'], JSON_UNESCAPED_UNICODE)));
            $response = $this->handle($defaultResponse, $message);

            if (!($response instanceof ResponseInterface)) {
                $response = $defaultResponse;
            }

            return ServerResponse::make($response);
        } catch (Exception $e) {
            return new Response(500, [], strval(json_encode(['code' => 'ERROR', 'message' => $e->getMessage()], JSON_UNESCAPED_UNICODE)));
        }
    }

    /**
     * @link https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_5.shtml
     *
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handlePaid(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->getEventType() === 'TRANSACTION.SUCCESS' && $message->trade_state === 'SUCCESS'
                ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    /**
     * @link https://pay.weixin.qq.com/wiki/doc/apiv3/apis/chapter3_1_11.shtml
     *
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleRefunded(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return in_array($message->getEventType(), [
                'REFUND.SUCCESS',
                'REFUND.ABNORMAL',
                'REFUND.CLOSED',
            ]) ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message|Message
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function getRequestMessage(ServerRequestInterface $request = null)
    {
        $originContent = (string)($request ?? $this->request)->getBody();

        // 微信支付的回调数据回调，偶尔是 XML https://github.com/w7corp/easywechat/issues/2737
        // PS: 这帮傻逼，真的是该死啊
        $isXml = str_starts_with($originContent, '<xml');
        $attributes = $isXml ? $this->decodeXmlMessage($originContent) : $this->decodeJsonMessage($originContent);

        return new Message($attributes, $originContent);
    }

    /**
     * @param string $contents
     * @return array
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    protected function decodeXmlMessage(string $contents): array
    {
        $attributes = Xml::parse($contents);

        if (!is_array($attributes)) {
            throw new RuntimeException('Invalid request body.');
        }

        if (!empty($attributes['req_info'])) {
            $key = $this->merchant->getV2SecretKey();

            if (empty($key)) {
                throw new InvalidArgumentException('V2 secret key is required.');
            }

            $attributes = Xml::parse(AesEcb::decrypt($attributes['req_info'], md5($key), ''));
        }

        if (!is_array($attributes)) {
            throw new RuntimeException('Failed to decrypt request message.');
        }

        return $attributes;
    }

    /**
     * @param string $contents
     * @return array
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    protected function decodeJsonMessage(string $contents): array
    {
        $attributes = json_decode($contents, true);

        if (!is_array($attributes)) {
            throw new RuntimeException('Invalid request body.');
        }

        if (empty($attributes['resource']['ciphertext'])) {
            throw new RuntimeException('Invalid request.');
        }

        $attributes = json_decode(
            AesGcm::decrypt(
                $attributes['resource']['ciphertext'],
                $this->merchant->getSecretKey(),
                $attributes['resource']['nonce'],
                $attributes['resource']['associated_data'],
            ),
            true
        );

        if (!is_array($attributes)) {
            throw new RuntimeException('Failed to decrypt request message.');
        }

        return $attributes;
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message|Message
     * @throws InvalidArgumentException
     * @throws RuntimeException
     */
    public function getDecryptedMessage(ServerRequestInterface $request = null)
    {
        return $this->getRequestMessage($request);
    }
}
