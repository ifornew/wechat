<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Pay;

use Ifornew\WeChat\Kernel\Exceptions\BadResponseException;
use Ifornew\WeChat\Kernel\HttpClient\Response as HttpClientResponse;
use Ifornew\WeChat\Pay\Contracts\Merchant as MerchantInterface;
use Psr\Http\Message\ResponseInterface as PsrResponse;

class ResponseValidator implements \Ifornew\WeChat\Pay\Contracts\ResponseValidator
{
    protected MerchantInterface $merchant;

    public function __construct(MerchantInterface $merchant)
    {
        $this->merchant = $merchant;
    }

    /**
     * @param PsrResponse|HttpClientResponse $response
     * @throws \Ifornew\WeChat\Kernel\Exceptions\BadResponseException
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidConfigException
     * @throws \Ifornew\WeChat\Pay\Exceptions\InvalidSignatureException
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function validate($response): void
    {
        if ($response instanceof HttpClientResponse) {
            $response = $response->toPsrResponse();
        }

        if ($response->getStatusCode() !== 200) {
            throw new BadResponseException('Request Failed');
        }

        (new Validator($this->merchant))->validate($response);
    }
}
