<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Pay;

use Ifornew\WeChat\Kernel\Contracts\Config as ConfigInterface;
use Ifornew\WeChat\Kernel\Contracts\Server as ServerInterface;
use Ifornew\WeChat\Kernel\Support\PrivateKey;
use Ifornew\WeChat\Kernel\Support\PublicKey;
use Ifornew\WeChat\Kernel\Traits\InteractWithConfig;
use Ifornew\WeChat\Kernel\Traits\InteractWithHttpClient;
use Ifornew\WeChat\Kernel\Traits\InteractWithServerRequest;
use Ifornew\WeChat\Pay\Contracts\Validator as ValidatorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class Application implements \Ifornew\WeChat\Pay\Contracts\Application
{
    use InteractWithConfig;
    use InteractWithHttpClient;
    use InteractWithServerRequest;

    protected ?ServerInterface $server = null;

    protected ?ValidatorInterface $validator = null;

    protected ?HttpClientInterface $client = null;

    protected ?Merchant $merchant = null;

    /**
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function getUtils(): Utils
    {
        return new Utils($this->getMerchant());
    }

    /**
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function getMerchant(): Merchant
    {
        if (!$this->merchant) {
            $this->merchant = new Merchant(
                $this->config['mch_id'], /** @phpstan-ignore-line */
                new PrivateKey((string)$this->config['private_key']), /** @phpstan-ignore-line */
                new PublicKey((string)$this->config['certificate']), /** @phpstan-ignore-line */
                (string)$this->config['secret_key'], /** @phpstan-ignore-line */
                (string)$this->config['v2_secret_key'], /** @phpstan-ignore-line */
                $this->config->has('platform_certs') ? (array)$this->config['platform_certs'] : []/** @phpstan-ignore-line */
            );
        }

        return $this->merchant;
    }

    /**
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function getValidator(): ValidatorInterface
    {
        if (!$this->validator) {
            $this->validator = new Validator($this->getMerchant());
        }

        return $this->validator;
    }

    /**
     * @param ValidatorInterface $validator
     * @return $this
     */
    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;

        return $this;
    }

    /**
     * @return Server|ServerInterface
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Throwable
     * @throws \ReflectionException
     */
    public function getServer()
    {
        if (!$this->server) {
            $this->server = new Server($this->getMerchant(), $this->getRequest());
        }

        return $this->server;
    }

    public function setServer(ServerInterface $server)
    {
        $this->server = $server;

        return $this;
    }

    public function setConfig(ConfigInterface $config)
    {
        $this->config = $config;

        return $this;
    }

    public function getConfig(): ConfigInterface
    {
        return $this->config;
    }

    /**
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function getClient(): HttpClientInterface
    {
        return $this->client ?? $this->client = (new Client(
                $this->getMerchant(),
                $this->getHttpClient(),
                (array)$this->config->get('http', [])
            ))->setPresets($this->config->all());
    }

    public function setClient(HttpClientInterface $client)
    {
        $this->client = $client;

        return $this;
    }
}
