<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Pay\Contracts;

use Ifornew\WeChat\Kernel\HttpClient\Response;
use Psr\Http\Message\ResponseInterface;

interface ResponseValidator
{
    /**
     * @param ResponseInterface|Response $response
     * @return void
     */
    public function validate($response): void;
}
