<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Pay\Contracts;

use Psr\Http\Message\MessageInterface;

interface Validator
{
    /**
     * @param MessageInterface $message
     * @throws \Ifornew\WeChat\Pay\Exceptions\InvalidSignatureException if signature validate failed.
     */
    public function validate(MessageInterface $message): void;
}
