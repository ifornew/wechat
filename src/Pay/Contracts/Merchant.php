<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Pay\Contracts;

use Ifornew\WeChat\Kernel\Support\PrivateKey;
use Ifornew\WeChat\Kernel\Support\PublicKey;

interface Merchant
{
    public function getMerchantId(): int;

    public function getPrivateKey(): PrivateKey;

    public function getSecretKey(): string;

    public function getV2SecretKey(): ?string;

    public function getCertificate(): PublicKey;

    public function getPlatformCert(string $serial): ?PublicKey;
}
