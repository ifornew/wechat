<?php

namespace Ifornew\WeChat\Pay\Exceptions;

use Ifornew\WeChat\Kernel\Exceptions\RuntimeException;

class InvalidSignatureException extends RuntimeException
{
}
