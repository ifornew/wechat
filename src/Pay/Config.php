<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Pay;

class Config extends \Ifornew\WeChat\Kernel\Config
{
    /**
     * @var array<string>
     */
    protected array $requiredKeys = [
        'mch_id',
        'secret_key',
        'private_key',
        'certificate',
    ];
}
