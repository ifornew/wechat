<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Kernel\Contracts;

interface Jsonable
{
    /**
     * @return string|false
     */
    public function toJson();
}
