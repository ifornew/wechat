<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Kernel\Contracts;

use Ifornew\WeChat\Kernel\Contracts\AccessToken as AccessTokenInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface AccessTokenAwareHttpClient extends HttpClientInterface
{
    public function withAccessToken(AccessTokenInterface $accessToken);
}
