<?php

namespace Ifornew\WeChat\Kernel\Contracts;

interface RefreshableJsApiTicket extends JsApiTicket
{
    public function refreshTicket(): string;
}
