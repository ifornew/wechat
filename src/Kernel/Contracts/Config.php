<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Kernel\Contracts;

use ArrayAccess;

/**
 * @extends ArrayAccess<string, mixed>
 */
interface Config extends ArrayAccess
{
    /**
     * @return array<string,mixed>
     */
    public function all(): array;

    public function has(string $key): bool;

    /**
     * @param string $key
     * @param mixed|null $value
     */
    public function set(string $key, $value = null): void;

    /**
     * @param array<string>|string $key
     * @param mixed|null $default
     * @return mixed
     */
    public function get($key, $default = null);
}
