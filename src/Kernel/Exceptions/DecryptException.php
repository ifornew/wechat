<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Kernel\Exceptions;

class DecryptException extends Exception
{
}
