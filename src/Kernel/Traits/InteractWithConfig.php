<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Kernel\Traits;

use Ifornew\WeChat\Kernel\Config;
use Ifornew\WeChat\Kernel\Contracts\Config as ConfigInterface;
use Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException;

use function is_array;

trait InteractWithConfig
{
    protected ConfigInterface $config;

    /**
     * @param array<string,mixed>|ConfigInterface $config
     *
     * @throws InvalidArgumentException
     */
    public function __construct($config)
    {
        $this->config = is_array($config) ? new Config($config) : $config;
    }

    public function getConfig(): ConfigInterface
    {
        return $this->config;
    }

    public function setConfig(ConfigInterface $config)
    {
        $this->config = $config;

        return $this;
    }
}
