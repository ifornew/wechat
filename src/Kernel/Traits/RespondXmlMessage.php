<?php

namespace Ifornew\WeChat\Kernel\Traits;

use Ifornew\WeChat\Kernel\Encryptor;
use Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException;
use Ifornew\WeChat\Kernel\Exceptions\RuntimeException;
use Ifornew\WeChat\Kernel\Message;
use Ifornew\WeChat\Kernel\Support\Xml;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;

use function array_merge;
use function is_array;
use function is_callable;
use function is_string;
use function time;

trait RespondXmlMessage
{
    /**
     * @param mixed $response
     * @return ResponseInterface
     * @throws RuntimeException
     * @throws InvalidArgumentException
     */
    public function transformToReply( $response, Message $message, Encryptor $encryptor = null): ResponseInterface
    {
        if (empty($response)) {
            return new Response(200, [], 'success');
        }

        return $this->createXmlResponse(
            array_filter(
                array_merge(
                    [
                        'ToUserName'   => $message->FromUserName,
                        'FromUserName' => $message->ToUserName,
                        'CreateTime'   => time(),
                    ],
                    $this->normalizeResponse($response),
                )
            ),
            $encryptor
        );
    }

    /**
     * @param mixed $response
     * @return array<string, mixed>
     *
     * @throws InvalidArgumentException
     */
    protected function normalizeResponse( $response): array
    {
        if (!is_string($response) && is_callable($response)) {
            $response = $response();
        }

        if (is_array($response)) {
            if (!isset($response['MsgType'])) {
                throw new InvalidArgumentException('MsgType cannot be empty.');
            }

            return $response;
        }

        if (is_string($response) || is_numeric($response)) {
            return [
                'MsgType' => 'text',
                'Content' => $response,
            ];
        }

        throw new InvalidArgumentException(
            sprintf('Invalid Response type "%s".', gettype($response))
        );
    }

    /**
     * @param array<string, mixed> $attributes
     *
     * @param Encryptor|null $encryptor
     * @return ResponseInterface
     * @throws RuntimeException
     */
    protected function createXmlResponse(array $attributes, Encryptor $encryptor = null): ResponseInterface
    {
        $xml = Xml::build($attributes);

        return new Response(200, ['Content-Type' => 'application/xml'], $encryptor ? $encryptor->encrypt($xml) : $xml);
    }
}
