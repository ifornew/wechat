<?php

namespace Ifornew\WeChat\Kernel\Traits;

use Ifornew\WeChat\Kernel\Encryptor;
use Ifornew\WeChat\Kernel\Exceptions\BadRequestException;
use Ifornew\WeChat\Kernel\Message;
use Ifornew\WeChat\Kernel\Support\Xml;

trait DecryptXmlMessage
{
    /**
     * @param int|string $timestamp
     * @throws \Ifornew\WeChat\Kernel\Exceptions\RuntimeException
     * @throws BadRequestException
     */
    public function decryptMessage(Message $message, Encryptor $encryptor, string $signature, $timestamp, string $nonce): Message
    {
        $ciphertext = $message->Encrypt;

        $this->validateSignature($encryptor->getToken(), $ciphertext, $signature, $timestamp, $nonce);

        $message->merge(Xml::parse($encryptor->decrypt($ciphertext, $signature, $nonce, $timestamp)) ?? []);

        return $message;
    }

    /**
     * @param int|string $timestamp
     * @throws BadRequestException
     */
    protected function validateSignature(string $token, string $ciphertext, string $signature, $timestamp, string $nonce): void
    {
        if (empty($signature)) {
            throw new BadRequestException('Request signature must not be empty.');
        }

        $params = [$token, $timestamp, $nonce, $ciphertext];

        sort($params, SORT_STRING);

        if ($signature !== sha1(implode($params))) {
            throw new BadRequestException('Invalid request signature.');
        }
    }
}
