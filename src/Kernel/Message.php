<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Kernel;

use ArrayAccess;
use Ifornew\WeChat\Kernel\Contracts\Jsonable;
use Ifornew\WeChat\Kernel\Exceptions\BadRequestException;
use Ifornew\WeChat\Kernel\Support\Xml;
use Ifornew\WeChat\Kernel\Traits\HasAttributes;
use Psr\Http\Message\ServerRequestInterface;

/**
 * @property string $FromUserName
 * @property string $ToUserName
 * @property string $Encrypt
 *
 * @implements ArrayAccess<array-key, mixed>
 */
abstract class Message implements ArrayAccess, Jsonable, \JsonSerializable
{
    use HasAttributes;

    protected ?string $originContent;

    /**
     * @param array<string,string> $attributes
     * @param string|null $originContent
     */
    final public function __construct(array $attributes = [], ?string $originContent = '')
    {
        $this->attributes = $attributes;
        $this->originContent = $originContent;
    }

    /**
     * @param ServerRequestInterface $request
     * @return Message
     * @throws BadRequestException
     */
    public static function createFromRequest(ServerRequestInterface $request): Message
    {
        $attributes = self::format($originContent = strval($request->getBody()));

        return new static($attributes, $originContent);
    }

    /**
     * @param string $originContent
     * @return array<string,string>
     *
     * @throws BadRequestException
     */
    public static function format(string $originContent): array
    {
        if (0 === stripos($originContent, '<')) {
            $attributes = Xml::parse($originContent);
        }

        // Handle JSON format.
        $dataSet = json_decode($originContent, true);

        if (JSON_ERROR_NONE === json_last_error() && $originContent) {
            $attributes = $dataSet;
        }

        if (empty($attributes) || !is_array($attributes)) {
            throw new BadRequestException('Failed to decode request contents.');
        }

        return $attributes;
    }

    public function getOriginalContents(): string
    {
        return $this->originContent ?? '';
    }

    public function __toString()
    {
        return $this->toJson() ?: '';
    }
}
