<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Kernel\HttpClient;

use Closure;
use Ifornew\WeChat\Kernel\Contracts\AccessToken as AccessTokenInterface;
use Ifornew\WeChat\Kernel\Contracts\AccessTokenAwareHttpClient as AccessTokenAwareHttpClientInterface;
use Ifornew\WeChat\Kernel\Traits\MockableHttpClient;
use Symfony\Component\HttpClient\AsyncDecoratorTrait;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use function array_merge;

/**
 * Class AccessTokenAwareClient.
 *
 *
 * @method HttpClientInterface withAppIdAs(string $name = null) 自定义 app_id 参数名
 * @method HttpClientInterface withAppId(string $value = null)
 */
class AccessTokenAwareClient implements AccessTokenAwareHttpClientInterface
{
    use AsyncDecoratorTrait;
    use HttpClientMethods;
    use RetryableClient;
    use MockableHttpClient;
    use RequestWithPresets;

    protected ?AccessTokenInterface $accessToken;
    protected ?Closure $failureJudge;
    protected bool $throw;

    public function __construct(HttpClientInterface $client = null, ?AccessTokenInterface $accessToken = null, ?Closure $failureJudge = null, bool $throw = true)
    {
        $this->client = $client ?? HttpClient::create();
        $this->accessToken = $accessToken;
        $this->failureJudge = $failureJudge;
        $this->throw = $throw;
    }

    public function withAccessToken(AccessTokenInterface $accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    /**
     * @param string $method
     * @param string $url
     * @param array<string, mixed> $options
     *
     * @return Response
     * @throws TransportExceptionInterface
     */
    public function request(string $method, string $url, array $options = []): Response
    {
        if ($this->accessToken) {
            $options['query'] = array_merge((array)($options['query'] ?? []), $this->accessToken->toQuery());
        }

        $options = RequestUtil::formatBody($this->mergeThenResetPrepends($options));

        return new Response($this->client->request($method, ltrim($url, '/'), $options), $this->failureJudge, $this->throw);
    }

    /**
     * @param string $name
     * @param array<int, mixed> $arguments
     * @return mixed
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException
     */
    public function __call(string $name, array $arguments)
    {
        if (\str_starts_with($name, 'with')) {
            return $this->handleMagicWithCall($name, $arguments[0] ?? null);
        }

        return $this->client->$name(...$arguments);
    }

    public static function createMockClient(MockHttpClient $mockHttpClient): HttpClientInterface
    {
        return new self($mockHttpClient);
    }
}
