<?php

namespace Ifornew\WeChat\Socialite\Exceptions\Feishu;

use Ifornew\WeChat\Socialite\Exceptions\Exception;

/**
 * Class BadRequestException.
 */
class InvalidTicketException extends Exception
{
}
