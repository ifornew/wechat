<?php

namespace Ifornew\WeChat\Socialite\Exceptions;

class MethodDoesNotSupportException extends Exception
{
    //
}
