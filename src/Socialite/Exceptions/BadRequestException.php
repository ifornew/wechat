<?php

namespace Ifornew\WeChat\Socialite\Exceptions;

/**
 * Class BadRequestException.
 */
class BadRequestException extends Exception
{
}
