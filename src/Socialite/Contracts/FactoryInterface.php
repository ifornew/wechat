<?php

namespace Ifornew\WeChat\Socialite\Contracts;

interface FactoryInterface
{
    /**
     * @param string $driver
     *
     * @return \Ifornew\WeChat\Socialite\Contracts\ProviderInterface
     */
    public function create(string $driver): ProviderInterface;
}
