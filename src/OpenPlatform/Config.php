<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenPlatform;

class Config extends \Ifornew\WeChat\Kernel\Config
{
    /**
     * @var array<string>
     */
    protected array $requiredKeys = [
        'app_id',
        'secret',
        'aes_key',
    ];
}
