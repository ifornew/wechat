<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenPlatform;

use Closure;
use Ifornew\WeChat\Kernel\Contracts\Server as ServerInterface;
use Ifornew\WeChat\Kernel\Encryptor;
use Ifornew\WeChat\Kernel\Exceptions\BadRequestException;
use Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException;
use Ifornew\WeChat\Kernel\Exceptions\RuntimeException;
use Ifornew\WeChat\Kernel\HttpClient\RequestUtil;
use Ifornew\WeChat\Kernel\ServerResponse;
use Ifornew\WeChat\Kernel\Traits\DecryptXmlMessage;
use Ifornew\WeChat\Kernel\Traits\InteractWithHandlers;
use Ifornew\WeChat\Kernel\Traits\RespondXmlMessage;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use function func_get_args;

class Server implements ServerInterface
{
    use InteractWithHandlers;
    use RespondXmlMessage;
    use DecryptXmlMessage;

    protected ?Closure $defaultVerifyTicketHandler = null;

    protected Encryptor $encryptor;

    protected ServerRequestInterface $request;

    /**
     * @param Encryptor $encryptor
     * @param ServerRequestInterface|null $request
     */
    public function __construct(Encryptor $encryptor, ServerRequestInterface $request = null)
    {
        $this->encryptor = $encryptor;
        $this->request = $request ?? RequestUtil::createDefaultServerRequest();
    }

    /**
     * @throws InvalidArgumentException
     * @throws BadRequestException
     * @throws RuntimeException
     */
    public function serve(): ResponseInterface
    {
        if ((bool)($str = $this->request->getQueryParams()['echostr'] ?? '')) {
            return new Response(200, [], $str);
        }

        $message = $this->getRequestMessage($this->request);

        $this->prepend($this->decryptRequestMessage());

        $response = $this->handle(new Response(200, [], 'success'), $message);

        if (!($response instanceof ResponseInterface)) {
            $response = $this->transformToReply($response, $message, $this->encryptor);
        }

        return ServerResponse::make($response);
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleAuthorized(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'authorized' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleUnauthorized(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'unauthorized' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleAuthorizeUpdated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'updateauthorized' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @throws InvalidArgumentException
     */
    public function withDefaultVerifyTicketHandler(callable $handler): void
    {
        $this->defaultVerifyTicketHandler = fn() => $handler(...func_get_args());
        $this->handleVerifyTicketRefreshed($this->defaultVerifyTicketHandler);
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleVerifyTicketRefreshed(callable $handler)
    {
        if ($this->defaultVerifyTicketHandler) {
            $this->withoutHandler($this->defaultVerifyTicketHandler);
        }

        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'component_verify_ticket' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    protected function decryptRequestMessage(): Closure
    {
        $query = $this->request->getQueryParams();

        return function (Message $message, Closure $next) use ($query) {
            $message = $this->decryptMessage($message, $this->encryptor, $query['msg_signature'] ?? '', $query['timestamp'] ?? '', $query['nonce'] ?? '');

            return $next($message);
        };
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message
     * @throws BadRequestException
     */
    public function getRequestMessage(ServerRequestInterface $request = null): \Ifornew\WeChat\Kernel\Message
    {
        return Message::createFromRequest($request ?? $this->request);
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message
     * @throws BadRequestException
     * @throws RuntimeException
     */
    public function getDecryptedMessage(ServerRequestInterface $request = null): \Ifornew\WeChat\Kernel\Message
    {
        $request = $request ?? $this->request;
        $message = $this->getRequestMessage($request);
        $query = $request->getQueryParams();

        return $this->decryptMessage($message, $this->encryptor, $query['msg_signature'] ?? '', $query['timestamp'] ?? '', $query['nonce'] ?? '');
    }
}
