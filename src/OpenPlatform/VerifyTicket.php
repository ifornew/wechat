<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenPlatform;

use Ifornew\WeChat\Kernel\Exceptions\RuntimeException;
use Ifornew\WeChat\OpenPlatform\Contracts\VerifyTicket as VerifyTicketInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Psr16Cache;

use function is_string;
use function sprintf;

class VerifyTicket implements VerifyTicketInterface
{
    protected string $appId;

    protected ?string $key;

    protected CacheInterface $cache;

    public function __construct(string $appId, ?string $key = null, CacheInterface $cache = null)
    {
        $this->appId = $appId;
        $this->key = $key;
        $this->cache = $cache ?? new Psr16Cache(new FilesystemAdapter('easywechat', 1500));
    }

    public function getKey(): string
    {
        return $this->key ?? $this->key = sprintf('open_platform.verify_ticket.%s', $this->appId);
    }

    public function setKey(string $key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function setTicket(string $ticket)
    {
        $this->cache->set($this->getKey(), $ticket, 6000);

        return $this;
    }

    /**
     * @throws RuntimeException
     * @throws InvalidArgumentException
     */
    public function getTicket(): string
    {
        $ticket = $this->cache->get($this->getKey());

        if (!$ticket || !is_string($ticket)) {
            throw new RuntimeException('No component_verify_ticket found.');
        }

        return $ticket;
    }
}
