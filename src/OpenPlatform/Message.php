<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenPlatform;

/**
 * @property string $InfoType
 * @property string $ComponentVerifyTicket
 */
class Message extends \Ifornew\WeChat\Kernel\Message
{
    //
}
