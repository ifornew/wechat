<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenPlatform\Contracts;

use Ifornew\WeChat\Kernel\Contracts\AccessToken;
use Ifornew\WeChat\Kernel\Contracts\Config;
use Ifornew\WeChat\Kernel\Contracts\Server;
use Ifornew\WeChat\Kernel\Encryptor;
use Ifornew\WeChat\Kernel\HttpClient\AccessTokenAwareClient;
use Ifornew\WeChat\MiniApp\Application as MiniAppApplication;
use Ifornew\WeChat\OfficialAccount\Application as OfficialAccountApplication;
use Ifornew\WeChat\OpenPlatform\AuthorizerAccessToken;
use Ifornew\WeChat\Socialite\Contracts\ProviderInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\SimpleCache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface Application
{
    public function getAccount(): Account;

    public function getEncryptor(): Encryptor;

    public function getServer(): Server;

    public function getRequest(): ServerRequestInterface;

    public function getClient(): AccessTokenAwareClient;

    public function getHttpClient(): HttpClientInterface;

    public function getConfig(): Config;

    public function getComponentAccessToken(): AccessToken;

    public function getCache(): CacheInterface;

    public function getOAuth(): ProviderInterface;

    /**
     * @param  array<string, mixed>  $config
     */
    public function getMiniApp(AuthorizerAccessToken $authorizerAccessToken, array $config): MiniAppApplication;

    /**
     * @param  array<string, mixed>  $config
     */
    public function getOfficialAccount(AuthorizerAccessToken $authorizerAccessToken, array $config): OfficialAccountApplication;
}
