<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenPlatform\Contracts;

interface VerifyTicket
{
    public function getTicket(): string;
}
