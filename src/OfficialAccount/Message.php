<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OfficialAccount;

/**
 * @property string $Event
 * @property string $MsgType
 */
class Message extends \Ifornew\WeChat\Kernel\Message
{
    //
}
