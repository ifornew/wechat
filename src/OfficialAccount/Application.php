<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OfficialAccount;

use Ifornew\WeChat\Kernel\Contracts\AccessToken as AccessTokenInterface;
use Ifornew\WeChat\Kernel\Contracts\JsApiTicket as JsApiTicketInterface;
use Ifornew\WeChat\Kernel\Contracts\RefreshableAccessToken as RefreshableAccessTokenInterface;
use Ifornew\WeChat\Kernel\Contracts\RefreshableJsApiTicket as RefreshableJsApiTicketInterface;
use Ifornew\WeChat\Kernel\Contracts\Server as ServerInterface;
use Ifornew\WeChat\Kernel\Encryptor;
use Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException;
use Ifornew\WeChat\Kernel\Exceptions\InvalidConfigException;
use Ifornew\WeChat\Kernel\HttpClient\AccessTokenAwareClient;
use Ifornew\WeChat\Kernel\HttpClient\AccessTokenExpiredRetryStrategy;
use Ifornew\WeChat\Kernel\HttpClient\RequestUtil;
use Ifornew\WeChat\Kernel\HttpClient\Response;
use Ifornew\WeChat\Kernel\Traits\InteractWithCache;
use Ifornew\WeChat\Kernel\Traits\InteractWithClient;
use Ifornew\WeChat\Kernel\Traits\InteractWithConfig;
use Ifornew\WeChat\Kernel\Traits\InteractWithHttpClient;
use Ifornew\WeChat\Kernel\Traits\InteractWithServerRequest;
use Ifornew\WeChat\OfficialAccount\Contracts\Account as AccountInterface;
use Ifornew\WeChat\OfficialAccount\Contracts\Application as ApplicationInterface;
use Ifornew\WeChat\Socialite\Contracts\ProviderInterface as SocialiteProviderInterface;
use Ifornew\WeChat\Socialite\Providers\WeChat;
use Psr\Log\LoggerAwareTrait;
use Symfony\Component\HttpClient\Response\AsyncContext;
use Symfony\Component\HttpClient\RetryableHttpClient;

use function array_merge;
use function call_user_func;
use function sprintf;
use function str_contains;

class Application implements ApplicationInterface
{
    use InteractWithConfig;
    use InteractWithCache;
    use InteractWithServerRequest;
    use InteractWithHttpClient;
    use InteractWithClient;
    use LoggerAwareTrait;

    protected ?Encryptor $encryptor = null;

    protected ?ServerInterface $server = null;

    protected ?AccountInterface $account = null;

    /**
     * @var AccessTokenInterface|RefreshableAccessTokenInterface|null $accessToken
     */
    protected $accessToken = null;

    protected ?JsApiTicketInterface $ticket = null;

    protected ?\Closure $oauthFactory = null;

    public function getAccount(): AccountInterface
    {
        if (!$this->account) {
            $this->account = new Account(
                (string)$this->config->get('app_id'), /** @phpstan-ignore-line */
                (string)$this->config->get('secret'), /** @phpstan-ignore-line */
                (string)$this->config->get('token'), /** @phpstan-ignore-line */
                (string)$this->config->get('aes_key')/** @phpstan-ignore-line */
            );
        }

        return $this->account;
    }

    public function setAccount(AccountInterface $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidConfigException
     */
    public function getEncryptor(): Encryptor
    {
        if (!$this->encryptor) {
            $token = $this->getAccount()->getToken();
            $aesKey = $this->getAccount()->getAesKey();

            if (empty($token) || empty($aesKey)) {
                throw new InvalidConfigException('token or aes_key cannot be empty.');
            }

            $this->encryptor = new Encryptor($this->getAccount()->getAppId(), $token, $aesKey, $this->getAccount()->getAppId());
        }

        return $this->encryptor;
    }

    public function setEncryptor(Encryptor $encryptor)
    {
        $this->encryptor = $encryptor;

        return $this;
    }

    /**
     * @return Server|ServerInterface
     * @throws InvalidArgumentException
     * @throws \Throwable
     * @throws \ReflectionException
     */
    public function getServer():Server
    {
        if (!$this->server) {
            $this->server = new Server($this->getRequest(), $this->getAccount()->getAesKey() ? $this->getEncryptor() : null);
        }

        return $this->server;
    }

    public function setServer(ServerInterface $server)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * @return AccessTokenInterface|RefreshableAccessTokenInterface
     */
    public function getAccessToken():AccessToken
    {
        if (!$this->accessToken) {
            $this->accessToken = new AccessToken($this->getAccount()->getAppId(), $this->getAccount()->getSecret(), null, $this->getCache(), $this->getHttpClient(), $this->config->get('use_stable_access_token', false));
        }

        return $this->accessToken;
    }

    /**
     * @param AccessTokenInterface|RefreshableAccessTokenInterface $accessToken
     * @return $this
     */
    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function setOAuthFactory(callable $factory)
    {
        $this->oauthFactory = fn(Application $app): WeChat => $factory($app);

        return $this;
    }

    /**
     * @throws InvalidArgumentException
     */
    public function getOAuth(): SocialiteProviderInterface
    {
        if (!$this->oauthFactory) {
            $this->oauthFactory = fn(self $app): SocialiteProviderInterface => (new WeChat(
                [
                    'client_id'     => $this->getAccount()->getAppId(),
                    'client_secret' => $this->getAccount()->getSecret(),
                    'redirect_url'  => $this->config->get('oauth.redirect_url'),
                ]
            ))->scopes((array)$this->config->get('oauth.scopes', ['snsapi_userinfo']));
        }

        $provider = call_user_func($this->oauthFactory, $this);

        if (!$provider instanceof SocialiteProviderInterface) {
            throw new InvalidArgumentException(sprintf(
                'The factory must return a %s instance.',
                SocialiteProviderInterface::class
            ));
        }

        return $provider;
    }

    /**
     * @return JsApiTicketInterface|RefreshableJsApiTicketInterface
     */
    public function getTicket()
    {
        if (!$this->ticket) {
            $this->ticket = new JsApiTicket($this->getAccount()->getAppId(), $this->getAccount()->getSecret(), null, $this->getCache(), $this->getClient(), $this->config->get('use_stable_access_token', false));
        }

        return $this->ticket;
    }

    /**
     * @param JsApiTicketInterface|RefreshableJsApiTicketInterface $ticket
     * @return $this
     */
    public function setTicket($ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    public function getUtils(): Utils
    {
        return new Utils($this);
    }

    public function createClient(): AccessTokenAwareClient
    {
        $httpClient = $this->getHttpClient();

        if ((bool)$this->config->get('http.retry', false)) {
            $httpClient = new RetryableHttpClient(
                $httpClient,
                $this->getRetryStrategy(),
                (int)$this->config->get('http.max_retries', 2) // @phpstan-ignore-line
            );
        }

        return (new AccessTokenAwareClient(
            $httpClient,
            $this->getAccessToken(),
            fn(Response $response) => (bool)($response->toArray()['errcode'] ?? 0),
            (bool)$this->config->get('http.throw', true)
        ))->setPresets($this->config->all());
    }

    public function getRetryStrategy(): AccessTokenExpiredRetryStrategy
    {
        $retryConfig = RequestUtil::mergeDefaultRetryOptions((array)$this->config->get('http.retry', []));

        return (new AccessTokenExpiredRetryStrategy($retryConfig))
            ->decideUsing(function (AsyncContext $context, ?string $responseContent): bool {
                return !empty($responseContent)
                    && str_contains($responseContent, '42001')
                    && str_contains($responseContent, 'access_token expired');
            });
    }

    /**
     * @return array<string,mixed>
     */
    protected function getHttpClientDefaultOptions(): array
    {
        return array_merge(
            ['base_uri' => 'https://api.weixin.qq.com/'],
            (array)$this->config->get('http', [])
        );
    }
}
