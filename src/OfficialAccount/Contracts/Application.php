<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OfficialAccount\Contracts;

use Ifornew\WeChat\Kernel\Contracts\AccessToken;
use Ifornew\WeChat\Kernel\Contracts\Config;
use Ifornew\WeChat\Kernel\Contracts\Server;
use Ifornew\WeChat\Kernel\Encryptor;
use Ifornew\WeChat\Kernel\HttpClient\AccessTokenAwareClient;
use Ifornew\WeChat\Socialite\Contracts\ProviderInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\SimpleCache\CacheInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

interface Application
{
    public function getAccount(): Account;

    public function getEncryptor(): Encryptor;

    public function getServer(): Server;

    public function getRequest(): ServerRequestInterface;

    public function getClient(): AccessTokenAwareClient;

    public function getHttpClient(): HttpClientInterface;

    public function getConfig(): Config;

    public function getAccessToken(): AccessToken;

    public function getCache(): CacheInterface;

    public function getOAuth(): ProviderInterface;

    public function setOAuthFactory(callable $factory);
}
