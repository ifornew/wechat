<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OfficialAccount;

use Closure;
use Ifornew\WeChat\Kernel\Contracts\Server as ServerInterface;
use Ifornew\WeChat\Kernel\Encryptor;
use Ifornew\WeChat\Kernel\Exceptions\BadRequestException;
use Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException;
use Ifornew\WeChat\Kernel\Exceptions\RuntimeException;
use Ifornew\WeChat\Kernel\HttpClient\RequestUtil;
use Ifornew\WeChat\Kernel\ServerResponse;
use Ifornew\WeChat\Kernel\Traits\DecryptXmlMessage;
use Ifornew\WeChat\Kernel\Traits\InteractWithHandlers;
use Ifornew\WeChat\Kernel\Traits\RespondXmlMessage;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class Server implements ServerInterface
{
    use RespondXmlMessage;
    use DecryptXmlMessage;
    use InteractWithHandlers;

    protected ServerRequestInterface $request;

    protected ?Encryptor $encryptor;

    /**
     * @param ServerRequestInterface|null $request
     * @param Encryptor|null $encryptor
     */
    public function __construct(ServerRequestInterface $request = null, ?Encryptor $encryptor = null)
    {
        $this->request = $request ?? RequestUtil::createDefaultServerRequest();
        $this->encryptor = $encryptor;
    }

    /**
     * @throws InvalidArgumentException
     * @throws BadRequestException
     * @throws RuntimeException
     */
    public function serve(): ResponseInterface
    {
        if ((bool)($str = $this->request->getQueryParams()['echostr'] ?? '')) {
            return new Response(200, [], $str);
        }

        $message = $this->getRequestMessage($this->request);
        $query = $this->request->getQueryParams();

        if ($this->encryptor && !empty($query['msg_signature'])) {
            $this->prepend($this->decryptRequestMessage($query));
        }

        $response = $this->handle(new Response(200, [], 'success'), $message);

        if (!($response instanceof ResponseInterface)) {
            $response = $this->transformToReply($response, $message, $this->encryptor);
        }

        return ServerResponse::make($response);
    }

    /**
     * @param string $type
     * @param callable|string $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function addMessageListener(string $type, $handler)
    {
        $handler = $this->makeClosure($handler);
        $this->withHandler(
            function (Message $message, Closure $next) use ($type, $handler) {
                return $message->MsgType === $type ? $handler($message, $next) : $next($message);
            }
        );

        return $this;
    }

    /**
     * @param string $event
     * @param callable|string $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public
    function addEventListener(string $event, $handler)
    {
        $handler = $this->makeClosure($handler);
        $this->withHandler(
            function (Message $message, Closure $next) use ($event, $handler) {
                return $message->Event === $event ? $handler($message, $next) : $next($message);
            }
        );

        return $this;
    }

    /**
     * @param array<string,string> $query
     *
     * @psalm-suppress PossiblyNullArgument
     */
    protected function decryptRequestMessage(array $query): Closure
    {
        return function (Message $message, Closure $next) use ($query) {
            if (!$this->encryptor) {
                return null;
            }

            $this->decryptMessage($message, $this->encryptor, $query['msg_signature'] ?? '', $query['timestamp'] ?? '', $query['nonce'] ?? '');

            return $next($message);
        };
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message
     * @throws BadRequestException
     */
    public function getRequestMessage(ServerRequestInterface $request = null): \Ifornew\WeChat\Kernel\Message
    {
        return Message::createFromRequest($request ?? $this->request);
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message
     * @throws BadRequestException
     * @throws RuntimeException
     */
    public function getDecryptedMessage(ServerRequestInterface $request = null): \Ifornew\WeChat\Kernel\Message
    {
        $request = $request ?? $this->request;
        $message = $this->getRequestMessage($request);
        $query = $request->getQueryParams();

        if (!$this->encryptor || empty($query['msg_signature'])) {
            return $message;
        }

        return $this->decryptMessage($message, $this->encryptor, $query['msg_signature'], $query['timestamp'] ?? '', $query['nonce'] ?? '');
    }
}
