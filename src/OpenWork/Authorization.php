<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenWork;

use ArrayAccess;
use Ifornew\WeChat\Kernel\Contracts\Arrayable;
use Ifornew\WeChat\Kernel\Contracts\Jsonable;
use Ifornew\WeChat\Kernel\Traits\HasAttributes;

/**
 * @implements ArrayAccess<string, mixed>
 */
class Authorization implements ArrayAccess, Jsonable, Arrayable
{
    use HasAttributes;

    public function getAppId(): string
    {
        /** @phpstan-ignore-next-line */
        return (string) $this->attributes['auth_corp_info']['corpid'];
    }
}
