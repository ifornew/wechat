<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenWork;

use Ifornew\WeChat\OpenWork\Contracts\Account as AccountInterface;

class Account implements AccountInterface
{
    protected string $corpId;

    protected string $providerSecret;

    protected string $suiteId;

    protected string $suiteSecret;

    protected string $token;

    protected string $aesKey;

    public function __construct(string $corpId, string $providerSecret, string $suiteId, string $suiteSecret, string $token, string $aesKey)
    {
        $this->corpId = $corpId;
        $this->providerSecret = $providerSecret;
        $this->suiteId = $suiteId;
        $this->suiteSecret = $suiteSecret;
        $this->token = $token;
        $this->aesKey = $aesKey;
    }

    public function getCorpId(): string
    {
        return $this->corpId;
    }

    public function getProviderSecret(): string
    {
        return $this->providerSecret;
    }

    public function getSuiteId(): string
    {
        return $this->suiteId;
    }

    public function getSuiteSecret(): string
    {
        return $this->suiteSecret;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getAesKey(): string
    {
        return $this->aesKey;
    }
}
