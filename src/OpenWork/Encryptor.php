<?php

namespace Ifornew\WeChat\OpenWork;

class Encryptor extends \Ifornew\WeChat\Kernel\Encryptor
{
    public function __construct(string $corpId, string $token, string $aesKey)
    {
        parent::__construct($corpId, $token, $aesKey, null);
    }
}
