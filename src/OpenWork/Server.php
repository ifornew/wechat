<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenWork;

use Closure;
use Ifornew\WeChat\Kernel\Contracts\Server as ServerInterface;
use Ifornew\WeChat\Kernel\Encryptor;
use Ifornew\WeChat\Kernel\Exceptions\BadRequestException;
use Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException;
use Ifornew\WeChat\Kernel\Exceptions\RuntimeException;
use Ifornew\WeChat\Kernel\HttpClient\RequestUtil;
use Ifornew\WeChat\Kernel\ServerResponse;
use Ifornew\WeChat\Kernel\Traits\DecryptXmlMessage;
use Ifornew\WeChat\Kernel\Traits\InteractWithHandlers;
use Ifornew\WeChat\Kernel\Traits\RespondXmlMessage;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

use function func_get_args;

class Server implements ServerInterface
{
    use InteractWithHandlers;
    use RespondXmlMessage;
    use DecryptXmlMessage;

    protected Encryptor $encryptor;

    protected Encryptor $providerEncryptor;

    protected ServerRequestInterface $request;

    protected ?Closure $defaultSuiteTicketHandler = null;

    /**
     * @param Encryptor $encryptor
     * @param Encryptor $providerEncryptor
     * @param ServerRequestInterface|null $request
     */
    public function __construct(Encryptor $encryptor, Encryptor $providerEncryptor, ServerRequestInterface $request = null)
    {
        $this->encryptor = $encryptor;
        $this->providerEncryptor = $providerEncryptor;
        $this->request = $request ?? RequestUtil::createDefaultServerRequest();
    }

    /**
     * @throws InvalidArgumentException
     * @throws BadRequestException
     * @throws RuntimeException
     */
    public function serve(): ResponseInterface
    {
        $query = $this->request->getQueryParams();

        if ((bool)($str = $query['echostr'] ?? '')) {
            $response = $this->providerEncryptor->decrypt(
                $str,
                $query['msg_signature'] ?? '',
                $query['nonce'] ?? '',
                $query['timestamp'] ?? ''
            );

            return new Response(200, [], $response);
        }

        $message = $this->getRequestMessage($this->request);

        $this->prepend($this->decryptRequestMessage());

        $response = $this->handle(new Response(200, [], 'success'), $message);

        if (!($response instanceof ResponseInterface)) {
            $response = $this->transformToReply($response, $message, $this->encryptor);
        }

        return ServerResponse::make($response);
    }

    /**
     * @param callable $handler
     * @throws InvalidArgumentException
     */
    public function withDefaultSuiteTicketHandler(callable $handler): void
    {
        $this->defaultSuiteTicketHandler = fn() => $handler(...func_get_args());
        $this->handleSuiteTicketRefreshed($this->defaultSuiteTicketHandler);
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleSuiteTicketRefreshed(callable $handler)
    {
        if ($this->defaultSuiteTicketHandler) {
            $this->withoutHandler($this->defaultSuiteTicketHandler);
        }

        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'suite_ticket' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    public function handleAuthCreated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'create_auth' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    public function handleAuthChanged(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_auth' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    public function handleAuthCancelled(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'cancel_auth' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    public function handleUserCreated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'create_user' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    public function handleUserUpdated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'update_user' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    public function handleUserDeleted(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'delete_user' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    public function handlePartyCreated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'create_party' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    public function handlePartyUpdated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'update_party' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    public function handlePartyDeleted(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'delete_party' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    public function handleUserTagUpdated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'update_tag' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    public function handleShareAgentChanged(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'share_agent_change' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    public function handleResetPermanentCode(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'reset_permanent_code' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    public function handleChangeAppAdmin(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->MsgType === 'event' && $message->Event === 'change_app_admin' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    protected function decryptRequestMessage(): Closure
    {
        $query = $this->request->getQueryParams();

        return function (Message $message, Closure $next) use ($query) {
            $this->decryptMessage(
                $message,
                $this->encryptor,
                $query['msg_signature'],
                $query['timestamp'],
                $query['nonce']
            );

            return $next($message);
        };
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message
     * @throws BadRequestException
     */
    public function getRequestMessage(ServerRequestInterface $request = null): \Ifornew\WeChat\Kernel\Message
    {
        return Message::createFromRequest($request ?? $this->request);
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message
     * @throws BadRequestException
     * @throws RuntimeException
     */
    public function getDecryptedMessage(ServerRequestInterface $request = null): \Ifornew\WeChat\Kernel\Message
    {
        $request = $request ?? $this->request;
        $message = $this->getRequestMessage($request);
        $query = $request->getQueryParams();

        return $this->decryptMessage($message, $this->encryptor, $query['msg_signature'] ?? '', $query['timestamp'] ?? '', $query['nonce'] ?? '');
    }
}
