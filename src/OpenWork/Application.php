<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenWork;

use Ifornew\WeChat\Kernel\Contracts\AccessToken as AccessTokenInterface;
use Ifornew\WeChat\Kernel\Contracts\Server as ServerInterface;
use Ifornew\WeChat\Kernel\Exceptions\HttpException;
use Ifornew\WeChat\Kernel\HttpClient\AccessTokenAwareClient;
use Ifornew\WeChat\Kernel\HttpClient\Response;
use Ifornew\WeChat\Kernel\Traits\InteractWithCache;
use Ifornew\WeChat\Kernel\Traits\InteractWithClient;
use Ifornew\WeChat\Kernel\Traits\InteractWithConfig;
use Ifornew\WeChat\Kernel\Traits\InteractWithHttpClient;
use Ifornew\WeChat\Kernel\Traits\InteractWithServerRequest;
use Ifornew\WeChat\OpenWork\Contracts\Account as AccountInterface;
use Ifornew\WeChat\OpenWork\Contracts\Application as ApplicationInterface;
use Ifornew\WeChat\OpenWork\Contracts\SuiteTicket as SuiteTicketInterface;
use Ifornew\WeChat\Socialite\Contracts\ProviderInterface as SocialiteProviderInterface;
use Ifornew\WeChat\Socialite\Providers\OpenWeWork;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

use function array_merge;

class Application implements ApplicationInterface
{
    use InteractWithCache;
    use InteractWithConfig;
    use InteractWithHttpClient;
    use InteractWithServerRequest;
    use InteractWithClient;

    protected ?ServerInterface $server = null;

    protected ?AccountInterface $account = null;

    protected ?Encryptor $encryptor = null;

    protected ?SuiteEncryptor $suiteEncryptor = null;

    protected ?SuiteTicketInterface $suiteTicket = null;

    protected ?AccessTokenInterface $accessToken = null;

    protected ?AccessTokenInterface $suiteAccessToken = null;

    protected ?AuthorizerAccessToken $authorizerAccessToken = null;

    public function getAccount(): AccountInterface
    {
        if (!$this->account) {
            $this->account = new Account(
                (string)$this->config->get('corp_id'), /** @phpstan-ignore-line */
                (string)$this->config->get('provider_secret'), /** @phpstan-ignore-line */
                (string)$this->config->get('suite_id'), /** @phpstan-ignore-line */
                (string)$this->config->get('suite_secret'), /** @phpstan-ignore-line */
                (string)$this->config->get('token'), /** @phpstan-ignore-line */
                (string)$this->config->get('aes_key')/** @phpstan-ignore-line */
            );
        }

        return $this->account;
    }

    public function setAccount(AccountInterface $account)
    {
        $this->account = $account;

        return $this;
    }

    public function getEncryptor(): Encryptor
    {
        if (!$this->encryptor) {
            $this->encryptor = new Encryptor($this->getAccount()->getCorpId(), $this->getAccount()->getToken(), $this->getAccount()->getAesKey());
        }

        return $this->encryptor;
    }

    public function setEncryptor(Encryptor $encryptor)
    {
        $this->encryptor = $encryptor;

        return $this;
    }

    public function getSuiteEncryptor(): SuiteEncryptor
    {
        if (!$this->suiteEncryptor) {
            $this->suiteEncryptor = new SuiteEncryptor($this->getAccount()->getSuiteId(), $this->getAccount()->getToken(), $this->getAccount()->getAesKey());
        }

        return $this->suiteEncryptor;
    }

    public function setSuiteEncryptor(SuiteEncryptor $encryptor)
    {
        $this->suiteEncryptor = $encryptor;

        return $this;
    }

    /**
     * @return Server|ServerInterface
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Throwable
     * @throws \ReflectionException
     */
    public function getServer(): Server
    {
        if (!$this->server) {
            $this->server = new Server($this->getSuiteEncryptor(), $this->getEncryptor(), $this->getRequest());

            $this->server->withDefaultSuiteTicketHandler(function (Message $message, \Closure $next) {
                if ($message->SuiteId === $this->getAccount()->getSuiteId()) {
                    $this->getSuiteTicket()->setTicket($message->SuiteTicket);
                }

                return $next($message);
            });
        }

        return $this->server;
    }

    public function setServer(ServerInterface $server)
    {
        $this->server = $server;

        return $this;
    }

    public function getProviderAccessToken(): AccessTokenInterface
    {
        if (!$this->accessToken) {
            $this->accessToken = new ProviderAccessToken(
                $this->getAccount()->getCorpId(),
                $this->getAccount()->getProviderSecret(), null,
                $this->getCache(),
                $this->getHttpClient()
            );
        }

        return $this->accessToken;
    }

    public
    function setProviderAccessToken(AccessTokenInterface $accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public
    function getSuiteAccessToken(): AccessTokenInterface
    {
        if (!$this->suiteAccessToken) {
            $this->suiteAccessToken = new SuiteAccessToken(
                $this->getAccount()->getSuiteId(),
                $this->getAccount()->getSuiteSecret(),
                $this->getSuiteTicket(), null,
                $this->getCache(),
                $this->getHttpClient()
            );
        }

        return $this->suiteAccessToken;
    }

    public
    function setSuiteAccessToken(AccessTokenInterface $accessToken)
    {
        $this->suiteAccessToken = $accessToken;

        return $this;
    }

    public
    function getSuiteTicket(): SuiteTicketInterface
    {
        if (!$this->suiteTicket) {
            $this->suiteTicket = new SuiteTicket($this->getAccount()->getSuiteId(), $this->getCache());
        }

        return $this->suiteTicket;
    }

    public
    function setSuiteTicket(SuiteTicketInterface $suiteTicket): SuiteTicketInterface
    {
        $this->suiteTicket = $suiteTicket;

        return $this->suiteTicket;
    }

    /**
     * @param string $corpId
     * @param string $permanentCode
     * @param AccessTokenInterface|null $suiteAccessToken
     * @return Authorization
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws HttpException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getAuthorization(string $corpId, string $permanentCode, AccessTokenInterface $suiteAccessToken = null): Authorization
    {
        $suiteAccessToken = $suiteAccessToken ?? $this->getSuiteAccessToken();

        $response = $this->getHttpClient()->request('POST', 'cgi-bin/service/get_auth_info', [
            'query' => [
                'suite_access_token' => $suiteAccessToken->getToken(),
            ],
            'json'  => [
                'auth_corpid'    => $corpId,
                'permanent_code' => $permanentCode,
            ],
        ])->toArray(false);

        if (empty($response['auth_corp_info'])) {
            throw new HttpException('Failed to get auth_corp_info: ' . json_encode($response, JSON_UNESCAPED_UNICODE));
        }

        return new Authorization($response);
    }

    /**
     * @param string $corpId
     * @param string $permanentCode
     * @param AccessTokenInterface|null $suiteAccessToken
     * @return AuthorizerAccessToken
     */
    public function getAuthorizerAccessToken(string $corpId, string $permanentCode, AccessTokenInterface $suiteAccessToken = null): AuthorizerAccessToken
    {
        $suiteAccessToken = $suiteAccessToken ?? $this->getSuiteAccessToken();

        return new AuthorizerAccessToken($corpId, $permanentCode, $suiteAccessToken, null, $this->getCache(), $this->getHttpClient());
    }

    public function createClient(): AccessTokenAwareClient
    {
        return (new AccessTokenAwareClient(
            $this->getHttpClient(),
            $this->getProviderAccessToken(),
            fn(Response $response) => (bool)($response->toArray()['errcode'] ?? 0),
            (bool)$this->config->get('http.throw', true)
        ))->setPresets($this->config->all());
    }

    /**
     * @param string $corpId
     * @param string $permanentCode
     * @param AccessTokenInterface|null $suiteAccessToken
     * @return AccessTokenAwareClient
     */
    public function getAuthorizerClient(string $corpId, string $permanentCode, AccessTokenInterface $suiteAccessToken = null): AccessTokenAwareClient
    {
        return (new AccessTokenAwareClient(
            $this->getHttpClient(),
            $this->getAuthorizerAccessToken($corpId, $permanentCode, $suiteAccessToken),
            fn(Response $response) => (bool)($response->toArray()['errcode'] ?? 0),
            (bool)$this->config->get('http.throw', true)
        ))->setPresets($this->config->all());
    }

    /**
     * @param string $corpId
     * @param string $permanentCode
     * @param AccessTokenInterface|null $suiteAccessToken
     * @return JsApiTicket
     */
    public function getJsApiTicket(string $corpId, string $permanentCode, AccessTokenInterface $suiteAccessToken = null): JsApiTicket
    {
        return new JsApiTicket($corpId, null, $this->getCache(), $this->getAuthorizerClient($corpId, $permanentCode, $suiteAccessToken));
    }

    public function getOAuth(string $suiteId, AccessTokenInterface $suiteAccessToken = null): SocialiteProviderInterface
    {
        $suiteAccessToken = $suiteAccessToken ?? $this->getSuiteAccessToken();

        return (new OpenWeWork(array_filter([
            'client_id'    => $suiteId,
            'redirect_url' => $this->config->get('oauth.redirect_url'),
            'base_url'     => $this->config->get('http.base_uri'),
        ])))->withSuiteTicket($this->getSuiteTicket()->getTicket())
            ->withSuiteAccessToken($suiteAccessToken->getToken())
            ->scopes((array)$this->config->get('oauth.scopes', ['snsapi_base']));
    }

    public function getCorpOAuth(string $corpId, AccessTokenInterface $suiteAccessToken = null): SocialiteProviderInterface
    {
        $suiteAccessToken = $suiteAccessToken ?? $this->getSuiteAccessToken();

        return (new OpenWeWork(array_filter([
            'client_id'    => $corpId,
            'redirect_url' => $this->config->get('oauth.redirect_url'),
            'base_url'     => $this->config->get('http.base_uri'),
        ])))->withSuiteTicket($this->getSuiteTicket()->getTicket())
            ->withSuiteAccessToken($suiteAccessToken->getToken())
            ->scopes((array)$this->config->get('oauth.scopes', ['snsapi_base']));
    }

    /**
     * @return array<string, mixed>
     */
    protected function getHttpClientDefaultOptions(): array
    {
        return array_merge(
            ['base_uri' => 'https://qyapi.weixin.qq.com/'],
            (array)$this->config->get('http', [])
        );
    }
}
