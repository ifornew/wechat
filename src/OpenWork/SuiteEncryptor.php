<?php

namespace Ifornew\WeChat\OpenWork;

use Ifornew\WeChat\Kernel\Encryptor;

class SuiteEncryptor extends Encryptor
{
    public function __construct(string $suiteId, string $token, string $aesKey)
    {
        parent::__construct($suiteId, $token, $aesKey, null);
    }
}
