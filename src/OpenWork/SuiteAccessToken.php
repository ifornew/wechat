<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenWork;

use const JSON_UNESCAPED_UNICODE;

use Ifornew\WeChat\Kernel\Contracts\RefreshableAccessToken as RefreshableAccessTokenInterface;
use Ifornew\WeChat\Kernel\Exceptions\HttpException;
use Ifornew\WeChat\OpenWork\Contracts\SuiteTicket as SuiteTicketInterface;
use Psr\SimpleCache\CacheInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Psr16Cache;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

use function abs;
use function intval;
use function json_encode;

class SuiteAccessToken implements RefreshableAccessTokenInterface
{
    protected string $suiteId;

    protected string $suiteSecret;

    protected ?SuiteTicketInterface $suiteTicket;

    protected ?string $key;

    protected HttpClientInterface $httpClient;

    protected CacheInterface $cache;

    public function __construct(string $suiteId, string $suiteSecret, ?SuiteTicketInterface $suiteTicket = null, ?string $key = null, CacheInterface $cache = null, HttpClientInterface $httpClient = null)
    {
        $this->suiteId = $suiteId;
        $this->suiteSecret = $suiteSecret;
        $this->suiteTicket = $suiteTicket;
        $this->key = $key;
        $this->httpClient = $httpClient ?? HttpClient::create(['base_uri' => 'https://qyapi.weixin.qq.com/']);
        $this->cache = $cache ?? new Psr16Cache(new FilesystemAdapter('easywechat', 1500));
        $this->suiteTicket ??= new SuiteTicket($this->suiteId, $this->cache);
    }

    public function getKey(): string
    {
        return $this->key ?? $this->key = \sprintf('open_work.suite_access_token.%s.%s', $this->suiteId, $this->suiteSecret);
    }

    public function setKey(string $key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @throws \Ifornew\WeChat\Kernel\Exceptions\HttpException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface|\Psr\SimpleCache\InvalidArgumentException
     */
    public function getToken(): string
    {
        $token = $this->cache->get($this->getKey());

        if ((bool)$token && \is_string($token)) {
            return $token;
        }

        return $this->refresh();
    }

    /**
     * @return array<string, string>
     *
     * @throws \Ifornew\WeChat\Kernel\Exceptions\HttpException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    #[ArrayShape(['suite_access_token' => 'string'])]
    public function toQuery(): array
    {
        return ['suite_access_token' => $this->getToken()];
    }

    /**
     * @throws \Ifornew\WeChat\Kernel\Exceptions\HttpException
     * @throws \Psr\SimpleCache\InvalidArgumentException
     * @throws \Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     * @throws \Ifornew\WeChat\Kernel\Exceptions\RuntimeException
     */
    public function refresh(): string
    {
        $response = $this->httpClient->request('POST', 'cgi-bin/service/get_suite_token', [
            'json' => [
                'suite_id'     => $this->suiteId,
                'suite_secret' => $this->suiteSecret,
                'suite_ticket' => $this->suiteTicket ? $this->suiteTicket->getTicket() : null
            ],
        ])->toArray(false);

        if (empty($response['suite_access_token'])) {
            throw new HttpException('Failed to get suite_access_token: ' . json_encode(
                    $response,
                    JSON_UNESCAPED_UNICODE
                ));
        }

        $this->cache->set(
            $this->getKey(),
            $response['suite_access_token'],
            abs(intval($response['expires_in']) - 100)
        );

        return $response['suite_access_token'];
    }
}
