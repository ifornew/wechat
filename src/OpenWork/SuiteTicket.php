<?php

declare(strict_types=1);

namespace Ifornew\WeChat\OpenWork;

use Ifornew\WeChat\Kernel\Exceptions\RuntimeException;
use Ifornew\WeChat\OpenWork\Contracts\SuiteTicket as SuiteTicketInterface;
use Psr\SimpleCache\CacheInterface;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Component\Cache\Psr16Cache;

use function is_string;
use function sprintf;

class SuiteTicket implements SuiteTicketInterface
{
    protected string $suiteId;

    protected CacheInterface $cache;

    protected ?string $key;

    public function __construct(string $suiteId, CacheInterface $cache = null, ?string $key = null)
    {
        $this->suiteId = $suiteId;
        $this->cache = $cache ?? new Psr16Cache(new FilesystemAdapter('easywechat', 1500));
        $this->key = $key;
    }

    public function getKey(): string
    {
        return $this->key ?? $this->key = sprintf('open_work.suite_ticket.%s', $this->suiteId);
    }

    public function setKey(string $key)
    {
        $this->key = $key;

        return $this;
    }

    /**
     * @param string $ticket
     * @return SuiteTicket
     * @throws InvalidArgumentException
     */
    public function setTicket(string $ticket)
    {
        $this->cache->set($this->getKey(), $ticket, 6000);

        return $this;
    }

    /**
     * @throws RuntimeException
     * @throws InvalidArgumentException
     */
    public function getTicket(): string
    {
        $ticket = $this->cache->get($this->getKey());

        if (!$ticket || !is_string($ticket)) {
            throw new RuntimeException('No suite_ticket found.');
        }

        return $ticket;
    }
}
