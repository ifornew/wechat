<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Work;

use Ifornew\WeChat\Work\Contracts\Account as AccountInterface;

class Account implements AccountInterface
{
    protected string $corpId;
    protected string $secret;
    protected string $token;
    protected string $aesKey;

    public function __construct(string $corpId, string $secret, string $token, string $aesKey)
    {
        $this->corpId = $corpId;
        $this->secret = $secret;
        $this->token = $token;
        $this->aesKey = $aesKey;
    }

    public function getCorpId(): string
    {
        return $this->corpId;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function getToken(): string
    {
        return $this->token;
    }

    public function getAesKey(): string
    {
        return $this->aesKey;
    }
}
