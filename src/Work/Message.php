<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Work;

/**
 * @property string $Event
 * @property string $InfoType
 * @property string $MsgType
 * @property string $ChangeType
 */
class Message extends \Ifornew\WeChat\Kernel\Message
{
    //
}
