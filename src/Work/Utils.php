<?php

namespace Ifornew\WeChat\Work;

use Ifornew\WeChat\Kernel\Exceptions\HttpException;
use Ifornew\WeChat\Kernel\Support\Str;
use Psr\SimpleCache\InvalidArgumentException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\DecodingExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

use function time;

class Utils
{
    protected Application $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * @param string $url
     * @param array<string> $jsApiList
     * @param array<string> $openTagList
     * @param bool $debug
     * @param bool $beta
     * @return array<string, mixed>
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws HttpException
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function buildJsSdkConfig(string $url, array $jsApiList, array $openTagList = [], bool $debug = false, bool $beta = true): array
    {
        return array_merge(
            compact('jsApiList', 'openTagList', 'debug', 'beta'),
            $this->app->getTicket()->createConfigSignature($url, Str::random(), time())
        );
    }

    /**
     * @param int $agentId
     * @param string $url
     * @param array<string> $jsApiList
     * @param array<string> $openTagList
     * @param bool $debug
     * @return array<string, mixed>
     *
     * @throws ClientExceptionInterface
     * @throws DecodingExceptionInterface
     * @throws HttpException
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function buildJsSdkAgentConfig(int $agentId, string $url, array $jsApiList, array $openTagList = [], bool $debug = false): array
    {
        return array_merge(
            compact('jsApiList', 'openTagList', 'debug'),
            $this->app->getTicket()->createAgentConfigSignature($agentId, $url, Str::random(), time())
        );
    }
}
