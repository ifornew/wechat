<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Work;

use Closure;
use Ifornew\WeChat\Kernel\Contracts\Server as ServerInterface;
use Ifornew\WeChat\Kernel\Encryptor;
use Ifornew\WeChat\Kernel\Exceptions\BadRequestException;
use Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException;
use Ifornew\WeChat\Kernel\Exceptions\RuntimeException;
use Ifornew\WeChat\Kernel\HttpClient\RequestUtil;
use Ifornew\WeChat\Kernel\ServerResponse;
use Ifornew\WeChat\Kernel\Traits\DecryptXmlMessage;
use Ifornew\WeChat\Kernel\Traits\InteractWithHandlers;
use Ifornew\WeChat\Kernel\Traits\RespondXmlMessage;
use Nyholm\Psr7\Response;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Throwable;

class Server implements ServerInterface
{
    use DecryptXmlMessage;
    use RespondXmlMessage;
    use InteractWithHandlers;

    protected Encryptor $encryptor;

    protected ServerRequestInterface $request;

    /**
     * @param Encryptor $encryptor
     * @param ServerRequestInterface|null $request
     */
    public function __construct(Encryptor $encryptor, ServerRequestInterface $request = null)
    {
        $this->encryptor = $encryptor;
        $this->request = $request ?? RequestUtil::createDefaultServerRequest();
    }

    /**
     * @throws InvalidArgumentException
     * @throws RuntimeException|BadRequestException|Throwable
     */
    public function serve(): ResponseInterface
    {
        $query = $this->request->getQueryParams();

        if (!empty($query['echostr'])) {
            $response = $this->encryptor->decrypt(
                $query['echostr'],
                $query['msg_signature'] ?? '',
                $query['nonce'] ?? '',
                $query['timestamp'] ?? ''
            );

            return new Response(200, [], $response);
        }

        $message = $this->getRequestMessage($this->request);

        $this->prepend($this->decryptRequestMessage());

        $response = $this->handle(new Response(200, [], 'SUCCESS'), $message);

        if (!($response instanceof ResponseInterface)) {
            $response = $this->transformToReply($response, $message, $this->encryptor);
        }

        return ServerResponse::make($response);
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleContactChanged(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->Event === 'change_contact' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleUserTagUpdated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->Event === 'change_contact' && $message->ChangeType === 'update_tag' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleUserCreated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->Event === 'change_contact' && $message->ChangeType === 'create_user' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleUserUpdated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->Event === 'change_contact' && $message->ChangeType === 'update_user' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleUserDeleted(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->Event === 'change_contact' && $message->ChangeType === 'delete_user' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handlePartyCreated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'create_party' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handlePartyUpdated(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'update_party' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handlePartyDeleted(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->InfoType === 'change_contact' && $message->ChangeType === 'delete_party' ? $handler(
                $message,
                $next
            ) : $next($message);
        });

        return $this;
    }

    /**
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function handleBatchJobsFinished(callable $handler)
    {
        $this->with(function (Message $message, Closure $next) use ($handler) {
            return $message->Event === 'batch_job_result' ? $handler($message, $next) : $next($message);
        });

        return $this;
    }

    /**
     * @param string $type
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function addMessageListener(string $type, callable $handler)
    {
        $this->withHandler(
            function (Message $message, Closure $next) use ($type, $handler) {
                return $message->MsgType === $type ? $handler($message, $next) : $next($message);
            }
        );

        return $this;
    }

    /**
     * @param string $event
     * @param callable $handler
     * @return Server
     * @throws InvalidArgumentException
     */
    public function addEventListener(string $event, callable $handler)
    {
        $this->withHandler(
            function (Message $message, Closure $next) use ($event, $handler) {
                return $message->Event === $event ? $handler($message, $next) : $next($message);
            }
        );

        return $this;
    }

    protected function validateUrl(): Closure
    {
        return function (Message $message, Closure $next): Response {
            $query = $this->request->getQueryParams();
            $response = $this->encryptor->decrypt(
                $query['echostr'],
                $query['msg_signature'] ?? '',
                $query['nonce'] ?? '',
                $query['timestamp'] ?? ''
            );

            return new Response(200, [], $response);
        };
    }

    protected function decryptRequestMessage(): Closure
    {
        return function (Message $message, Closure $next) {
            $query = $this->request->getQueryParams();
            $this->decryptMessage(
                $message,
                $this->encryptor,
                $query['msg_signature'] ?? '',
                $query['timestamp'] ?? '',
                $query['nonce'] ?? ''
            );

            return $next($message);
        };
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message
     * @throws BadRequestException
     */
    public function getRequestMessage(ServerRequestInterface $request = null): \Ifornew\WeChat\Kernel\Message
    {
        return Message::createFromRequest($request ?? $this->request);
    }

    /**
     * @param ServerRequestInterface|null $request
     * @return \Ifornew\WeChat\Kernel\Message
     * @throws BadRequestException
     * @throws RuntimeException
     */
    public function getDecryptedMessage(ServerRequestInterface $request = null): \Ifornew\WeChat\Kernel\Message
    {
        $request = $request ?? $this->request;
        $message = $this->getRequestMessage($request);
        $query = $request->getQueryParams();

        return $this->decryptMessage($message, $this->encryptor, $query['msg_signature'] ?? '', $query['timestamp'] ?? '', $query['nonce'] ?? '');
    }
}
