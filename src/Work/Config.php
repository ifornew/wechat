<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Work;

class Config extends \Ifornew\WeChat\Kernel\Config
{
    /**
     * @var array<string>
     */
    protected array $requiredKeys = [
        'corp_id',
        'secret',
        'token',
        'aes_key',
    ];
}
