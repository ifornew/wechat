<?php

declare(strict_types=1);

namespace Ifornew\WeChat\Work;

use Ifornew\WeChat\Kernel\Contracts\AccessToken as AccessTokenInterface;
use Ifornew\WeChat\Kernel\Contracts\Server as ServerInterface;
use Ifornew\WeChat\Kernel\HttpClient\AccessTokenAwareClient;
use Ifornew\WeChat\Kernel\HttpClient\Response;
use Ifornew\WeChat\Kernel\Traits\InteractWithCache;
use Ifornew\WeChat\Kernel\Traits\InteractWithClient;
use Ifornew\WeChat\Kernel\Traits\InteractWithConfig;
use Ifornew\WeChat\Kernel\Traits\InteractWithHttpClient;
use Ifornew\WeChat\Kernel\Traits\InteractWithServerRequest;
use Ifornew\WeChat\Work\Contracts\Account as AccountInterface;
use Ifornew\WeChat\Work\Contracts\Application as ApplicationInterface;
use Ifornew\WeChat\Socialite\Contracts\ProviderInterface as SocialiteProviderInterface;
use Ifornew\WeChat\Socialite\Providers\WeWork;

use function array_merge;

class Application implements ApplicationInterface
{
    use InteractWithConfig;
    use InteractWithCache;
    use InteractWithServerRequest;
    use InteractWithHttpClient;
    use InteractWithClient;

    protected ?Encryptor $encryptor = null;

    protected ?ServerInterface $server = null;

    protected ?AccountInterface $account = null;

    protected ?JsApiTicket $ticket = null;

    protected ?AccessTokenInterface $accessToken = null;

    public function getAccount(): AccountInterface
    {
        if (!$this->account) {
            $this->account = new Account(
                (string)$this->config->get('corp_id'), /** @phpstan-ignore-line */
                (string)$this->config->get('secret'), /** @phpstan-ignore-line */
                (string)$this->config->get('token'), /** @phpstan-ignore-line */
                (string)$this->config->get('aes_key')/** @phpstan-ignore-line */
            );
        }

        return $this->account;
    }

    public function setAccount(AccountInterface $account)
    {
        $this->account = $account;

        return $this;
    }

    public function getEncryptor(): Encryptor
    {
        if (!$this->encryptor) {
            $this->encryptor = new Encryptor($this->getAccount()->getCorpId(), $this->getAccount()->getToken(), $this->getAccount()->getAesKey());
        }

        return $this->encryptor;
    }

    public function setEncryptor(Encryptor $encryptor)
    {
        $this->encryptor = $encryptor;

        return $this;
    }

    /**
     * @return Server|ServerInterface
     * @throws \Ifornew\WeChat\Kernel\Exceptions\InvalidArgumentException
     * @throws \Throwable
     * @throws \ReflectionException
     */
    public function getServer():Server
    {
        if (!$this->server) {
            $this->server = new Server($this->getEncryptor(), $this->getRequest());
        }

        return $this->server;
    }

    public function setServer(ServerInterface $server)
    {
        $this->server = $server;

        return $this;
    }

    public function getAccessToken(): AccessTokenInterface
    {
        if (!$this->accessToken) {
            $this->accessToken = new AccessToken($this->getAccount()->getCorpId(), $this->getAccount()->getSecret(), null, $this->getCache(), $this->getHttpClient());
        }

        return $this->accessToken;
    }

    public function setAccessToken(AccessTokenInterface $accessToken)
    {
        $this->accessToken = $accessToken;

        return $this;
    }

    public function getUtils(): Utils
    {
        return new Utils($this);
    }

    public function createClient(): AccessTokenAwareClient
    {
        return (new AccessTokenAwareClient($this->getHttpClient(), $this->getAccessToken(), fn(Response $response) => (bool)($response->toArray()['errcode'] ?? 0), (bool)$this->config->get('http.throw', true)))->setPresets($this->config->all());
    }

    public function getOAuth(): SocialiteProviderInterface
    {
        $provider = new WeWork(
            [
                'client_id'     => $this->getAccount()->getCorpId(),
                'client_secret' => $this->getAccount()->getSecret(),
                'redirect_url'  => $this->config->get('oauth.redirect_url'),
            ]
        );

        $provider->withApiAccessToken($this->getAccessToken()->getToken());
        $provider->scopes((array)$this->config->get('oauth.scopes', ['snsapi_base']));

        if ($this->config->has('agent_id') && \is_numeric($this->config->get('agent_id'))) {
            $provider->withAgentId((int)$this->config->get('agent_id'));
        }

        return $provider;
    }

    public function getTicket(): JsApiTicket
    {
        if (!$this->ticket) {
            $this->ticket = new JsApiTicket($this->getAccount()->getCorpId(), null, $this->getCache(), $this->getClient(),);
        }

        return $this->ticket;
    }

    public function setTicket(JsApiTicket $ticket)
    {
        $this->ticket = $ticket;

        return $this;
    }

    /**
     * @return array<string, mixed>
     */
    protected function getHttpClientDefaultOptions(): array
    {
        return array_merge(
            ['base_uri' => 'https://qyapi.weixin.qq.com/'],
            (array)$this->config->get('http', [])
        );
    }
}
